<?php

/**
 * @author Filip Nowacki <hello@nowacki.it>
 */
namespace UnitedPrototype\GoogleExperiments;

final class Experiment
{
    const ORIGINAL_VARIATION = 0;
    const NO_CHOSEN_VARIATION = -1;
    const NOT_PARTICIPATING = -2;

    const CX_URL = 'http://www.google-analytics.com/cx/api.js';

    /**
     * @var string
     */
    private $key;

    /**
     * @var array
     */
    private $data;

    /**
     * @var int
     */
    private $connectionTimeout = 2;

    /**
     * @var int
     */
    private $requestTimeout = 2;

    /**
     * @var string
     */
    private $domainName = 'auto';

    /**
     * @var string
     */
    private $cookiePath = '/';

    /**
     * @var int
     */
    private $cookieExpirationInSeconds = 48211200;

    /**
     * Optional. A cache directory that will be used to cache
     * the requests being made to Google servers
     *
     * @var string
     */
    private $cacheDir;

    /**
     * @var int
     */
    private $cacheTtl = 60;

    /**
     * @var bool
     */
    private $cacheLocking = true;

    /**
     * @param string $key Generated in Google Analytics Experiment key
     *
     * @throws \Exception
     */
    public function __construct($key = '')
    {
        if (!extension_loaded('curl')) {
            throw new \Exception('Google Experiments requires the cURL extension to be installed.');
        }

        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @return string
     * @throws \UnexpectedValueException
     */
    public function getDomainName()
    {
        if ($this->domainName == 'auto') {
            return isset($_SERVER['HTTP_HOST']) ? strtolower($_SERVER['HTTP_HOST']) : null;
        }

        if (!empty($this->domainName)) {
            return $this->domainName;
        }

        throw new \UnexpectedValueException('Unable to detect domain name, provide one by setDomainName method');
    }

    /**
     * @param string $domainName
     */
    public function setDomainName($domainName)
    {
        $this->domainName = strtolower($domainName);
    }

    /**
     * @return array
     */
    private function getData()
    {
        return empty($this->data) ? $this->data = $this->loadData() : $this->data;
    }

    /**
     * @return int
     */
    public function getConnectionTimeout()
    {
        return $this->connectionTimeout;
    }

    /**
     * @param int $connectionTimeout
     */
    public function setConnectionTimeout($connectionTimeout)
    {
        $this->connectionTimeout = $connectionTimeout;
    }

    /**
     * @return int
     */
    public function getRequestTimeout()
    {
        return $this->requestTimeout;
    }

    /**
     * @param int $requestTimeout
     */
    public function setRequestTimeout($requestTimeout)
    {
        $this->requestTimeout = $requestTimeout;
    }

    /**
     * @return string
     */
    public function getCookiePath()
    {
        return $this->cookiePath;
    }

    /**
     * @param string $cookiePath
     */
    public function setCookiePath($cookiePath)
    {
        $this->cookiePath = $cookiePath;
    }

    /**
     * @return int
     */
    public function getCookieExpirationInSeconds()
    {
        return $this->cookieExpirationInSeconds;
    }

    /**
     * @param int $cookieExpirationInSeconds
     */
    public function setCookieExpirationInSeconds($cookieExpirationInSeconds)
    {
        $this->cookieExpirationInSeconds = $cookieExpirationInSeconds;
    }

    /**
     * @return string
     */
    public function getCacheDir()
    {
        return $this->cacheDir;
    }

    /**
     * @param string $cacheDir
     */
    public function setCacheDir($cacheDir)
    {
        $this->cacheDir = rtrim($cacheDir, DIRECTORY_SEPARATOR);
    }

    /**
     * @return int
     */
    public function getCacheTtl()
    {
        return $this->cacheTtl;
    }

    /**
     * @param int $cacheTtl
     */
    public function setCacheTtl($cacheTtl)
    {
        $this->cacheTtl = $cacheTtl;
    }

    /**
     * @return bool
     */
    public function isCacheLocking()
    {
        return $this->cacheLocking;
    }

    /**
     * @param bool $cacheLocking
     */
    public function setCacheLocking($cacheLocking)
    {
        $this->cacheLocking = $cacheLocking;
    }

    /**
     * Retrieves experiment data from Google servers
     * This allows us to take the different variation weights into account to make use of
     * the multi-armed bandit algorithm (https://support.google.com/analytics/answer/2844870).
     *
     * @return array
     * @throws \Exception
     */
    private function loadData()
    {
        $response = null;
        $cacheDir = $this->getCacheDir();
        $cachePath = null;
        $fp = null;
        $error = null;
        $response = null;

        if ($cacheDir) {
            $cachePath = $cacheDir . DIRECTORY_SEPARATOR . 'abtests-' . rawurlencode($this->key) . '.cache';
            if (is_readable($cachePath) && time() <= filemtime($cachePath) + $this->getCacheTtl()) {
                $fp = fopen($cachePath, 'r');
                if ($this->isCacheLocking()) {
                    flock($fp, LOCK_SH);
                }
                $response = stream_get_contents($fp);
                flock($fp, LOCK_UN);
                fclose($fp);
            }
        }

        if (empty($response)) {
            if ($cacheDir) {
                @mkdir($cacheDir, 0777, true);

                $fp = fopen($cachePath, 'c');
                if ($this->isCacheLocking()) {
                    flock($fp, LOCK_EX);
                }
            }

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $this->getConnectionTimeout());
            curl_setopt($curl, CURLOPT_TIMEOUT, $this->getRequestTimeout());
            curl_setopt($curl, CURLOPT_FAILONERROR, true);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_VERBOSE, false);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($curl, CURLOPT_USERAGENT, '');
            curl_setopt($curl, CURLOPT_URL, self::CX_URL . '?experiment=' . rawurlencode($this->key));
            $response = curl_exec($curl);
            $error = curl_error($curl);
            curl_close($curl);

            if ($cacheDir && $response) {
                if (!is_writable($cacheDir)) {
                    throw new Exception('Cache directory "' . $cacheDir . '" is not writable.');
                }

                ftruncate($fp, 0);
                fwrite($fp, $response);
                fflush($fp);
            }

            if ($cacheDir) {
                flock($fp, LOCK_UN);
                fclose($fp);
            }
        }

        if (!$response) {
            throw new \Exception('Unable to retrieve Google Analytics Content Experiments API response: ' . (isset($error) ? $error : '') . (strlen($response) ? "\n" . $response : ''));
        } else {
            if (!preg_match('#\.experiments_\s*=\s*(\{(?:[^{}]+|(?1))+\})#', $response, $m)) {
                throw new \UnexpectedValueException('Unable to find experiments in Google Analytics Content Experiments API response.');
            } else {
                $experiments = json_decode($m[1], true);
                if ($experiments === null) {
                    throw new \UnexpectedValueException('Unable to parse JSON from Google Analytics Content Experiments API response.');
                } else {
                    if (isset($experiments[$this->key]['data'])) {
                        return $experiments[$this->key]['data'];
                    } elseif (isset($experiments[$this->key]['error'])) {
                        throw new \UnexpectedValueException('Error from Google Analytics Content Experiments API: ' . $experiments[$this->key]['error']['code'] . ' - ' . $experiments[$this->key]['error']['message']);
                    } else {
                        throw new \UnexpectedValueException('Unable to find experiment data in JSON from Google Analytics Content Experiments API response.');
                    }
                }
            }
        }
    }

    /**
     * @link https://developers.google.com/analytics/devguides/collection/gajs/experiments#cxjs-methods
     *
     * @param  string $utmx Value of the "__utmx" cookie. Defaults to $_COOKIE['__utmx'] if defined
     * @param  string $utmxx Value of the "__utmxx" cookie. Defaults to $_COOKIE['__utmxx'] if defined
     * @param  bool   $setCookies See setChosenVariation() - sets the "__utmx" and "__utmxx" cookies with
     *                              setrawcookie() if true
     *
     * @return int
     */
    public function chooseVariation($utmx = null, $utmxx = null, $setCookies = true)
    {
        $variation = $this->getChosenVariation($utmx);

        if ($variation === null) {
            $variation = $this->chooseNewVariation();
            $this->setChosenVariation($variation, $utmx, $utmxx, $setCookies);
        }

        return $variation;
    }

    /**
     * @return int
     */
    public function chooseNewVariation()
    {
        $data = $this->getData();

        $rand = mt_rand(0, 1E9) / 1E9;
        foreach ($data['items'] as $item) {
            if (isset($item['weight']) && empty($item['disabled'])) {
                if ($rand < $item['weight']) {
                    if ($item['id'] !== null) {
                        return (int)$item['id'];
                    } else {
                        return self::NOT_PARTICIPATING;
                    }
                }
                $rand -= $item['weight'];
            }
        }

        return self::ORIGINAL_VARIATION;
    }

    /**
     * @link https://developers.google.com/analytics/devguides/collection/gajs/experiments#cxjs-methods
     *
     * @param  string $utmx Value of the "__utmx" cookie. Defaults to $_COOKIE['__utmx'] if defined
     *
     * @return int|null
     */
    public function getChosenVariation($utmx = null)
    {
        if ($utmx === null && isset($_COOKIE['__utmx'])) {
            $utmx = $_COOKIE['__utmx'];
        }

        $experiments = explode('.', $utmx);
        if (count($experiments) > 1) {
            array_shift($experiments);

            foreach ($experiments as $experiment) {
                if (preg_match('/^([^$]+)\$([^:]+):(.*)$/', $experiment, $m)) {
                    if ($m[1] == $this->id) {
                        $variations = explode('-', $m[3]);
                        return (int)$variations[0];
                    }
                }
            }
        }

        return null;
    }

    /**
     * @link https://developers.google.com/analytics/devguides/collection/gajs/experiments#cxjs-methods
     *
     * @param  int    $variation Variation number
     * @param  string $utmx Value of the "__utmx" cookie. Defaults to $_COOKIE['__utmx'] if defined
     * @param  string $utmxx Value of the "__utmxx" cookie. Defaults to $_COOKIE['__utmxx'] if defined
     * @param  bool   $setCookies Sets the "__utmx" and "__utmxx" cookies with setrawcookie() if true
     *
     * @return array                Returns an array of cookies to set so you can set the cookies thru your
     *                              framework's methods if you set $setCookies to false.
     */
    public function setChosenVariation($variation, $utmx = null, $utmxx = null, $setCookies = true)
    {
        if ($utmx === null && isset($_COOKIE['__utmx'])) {
            $utmx = $_COOKIE['__utmx'];
        }
        if ($utmxx === null && isset($_COOKIE['__utmxx'])) {
            $utmxx = $_COOKIE['__utmxx'];
        }

        $domainHash = static::generateHash($this->getDomainName());

        $cookies = [
            '__utmx'  => [
                'value'    => $utmx,
                'expire'   => time() + $this->getCookieExpirationInSeconds(),
                'path'     => $this->getCookiePath(),
                'domain'   => '.' . $this->getDomainName(),
                'secure'   => false,
                'httponly' => false,
            ],
            '__utmxx' => [
                'value'    => $utmxx,
                'expire'   => time() + $this->getCookieExpirationInSeconds(),
                'path'     => $this->getCookiePath(),
                'domain'   => '.' . $this->getDomainName(),
                'secure'   => false,
                'httponly' => false,
            ],
        ];

        $experiments = explode('.', $utmx);
        if (count($experiments) > 1) {
            $domainHash = array_shift($experiments);
            $found = false;
            foreach ($experiments as &$experiment) {
                if (preg_match('/^([^$]+)\$([^:]+):(.*)$/', $experiment, $m)) {
                    if ($m[1] == $this->id) {
                        $found = true;
                        $experiment = $m[1] . '$' . $m[2] . ':' . $variation;
                    }
                }
            }
            if (!$found) {
                $experiments[] = $this->id . '$0' . ':' . $variation;
            }
            $cookies['__utmx']['value'] = $domainHash . '.' . implode('.', $experiments);
        } else {
            $cookies['__utmx']['value'] = $domainHash . '.' . $this->id . '$0' . ':' . $variation;
        }

        $experiments = explode('.', $utmxx);
        if (count($experiments) > 1) {
            $domainHash = array_shift($experiments);
            $found = false;
            foreach ($experiments as &$experiment) {
                if (preg_match('/^([^$]+)\$([^:]+):([^:]+):([^:]+):?(.*)$/', $experiment, $m)) {
                    if ($m[1] == $this->id) {
                        $found = true;
                        $experiment = $m[1] . '$' . $m[2] . ':' . time() . ':' . $m[4] . ($m[5] ? ':' . $m[5] : '');
                    }
                }
            }
            if (!$found) {
                $experiments[] = $this->id . '$0' . ':' . time() . ':' . 8035200;
            }
            $cookies['__utmxx']['value'] = $domainHash . '.' . implode('.', $experiments);
        } else {
            $cookies['__utmxx']['value'] = $domainHash . '.' . $this->id . '$0' . ':' . time() . ':' . 8035200;
        }

        if ($setCookies) {
            foreach ($cookies as $name => $cookie) {
                setrawcookie($name, $cookie['value'], $cookie['expire'], $cookie['path'], $cookie['domain'], $cookie['secure'], $cookie['httponly']);
            }
        }

        return $cookies;
    }

    /**
     * @param  string $string
     *
     * @return int
     */
    protected static function generateHash($string)
    {
        $string = (string)$string;
        $hash = 1;

        if ($string !== null && $string !== '') {
            $hash = 0;

            $length = strlen($string);
            for ($pos = $length - 1; $pos >= 0; $pos--) {
                $current = ord($string[$pos]);
                $hash = (($hash << 6) & 0xfffffff) + $current + ($current << 14);
                $leftMost7 = $hash & 0xfe00000;
                if ($leftMost7 != 0) {
                    $hash ^= $leftMost7 >> 21;
                }
            }
        }

        return $hash;
    }
}
