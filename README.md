# Server Side Google Experiments
### About
Server Side Google Experiments is basically Google's cx/api.js in PHP: A server-side implementation that allows you to control and implement Google Experiments entirely on the server.

This is done by parsing experiment data to make use of GA's multi-armed bandit algorithm as well as programmatically modifying the "__utmx" and "__utmxx" cookies.

This does however depend on Google Analytics being used in the browser, see "Usage" below.

This library might become obsolete as soon as Google implements Content Experiments into their [Measurement Protocol](https://developers.google.com/analytics/devguides/collection/protocol/v1/).

### Requirements
Requires PHP 5.3+ as namespaces and closures are used. Has no other dependencies and can be used independantly from any framework or whatsoever environment.

cURL must be enabled.

### Usage example
All methods match the ones from the JS API, so using Server Side Google Experiments is pretty straightforward if you've experience with [cx/api.js](https://developers.google.com/analytics/devguides/collection/gajs/experiments#cxjs):

```php
use UnitedPrototype\GoogleExperiments;

$experiment = new GoogleExperiments\Experiment('your-generated-key');

$variation = $experiment->chooseVariation();
```

In order to have the experiment data transferred to Google Analytics, you need to use Google Analytics on the client side either via the traditional ga.js or Google's new analytics.js (Universal Analytics).

ga.js will work out of the box, as it simply consider and include the "__utmx" cookie value when sending tracking data to Google Analytics.

analytics.js does sadly no longer consider the "__utmx" cookie. You will therefore have to tell it via Javascript which variation of an experiment has been chosen, and it's limited to one experiment per page. Example:

```javascript
// Hand over experiment data to analytics.js, as it ignores the "__utmx" cookie
window.gaData = {
	expId:  '<?= $experiment->getId(); ?>',
	expVar: '<?= $experiment->getChosenVariation(); ?>'
};
```

The advantage of using Server Side Google Experiments here is still that you can choose the variation on the server-side while still making use of the multi-armed bandit algorithm considering the variation's different weights.

### Development plans
- Add unit tests
- Refactor